angular.module("pokemonApp", [])
  .controller("SearchController", ['$http', '$scope', function ($http, $scope) {
  
    $scope.pokemons = null;
    $scope.allPokemons = false;
    $scope.nonefound = false;
    $scope.pokemonfound = false;
    $scope.pokemonInfo = {};
    $scope.previous = null;
    $scope.next = null;
    $scope.searchTerm = "";
    $scope.sortListAlphabetically = false;
      
    this.search = function searchPokemons() {
      let url = 'https://pokeapi.co/api/v2/pokemon/' + $scope.searchTerm;
      
      return $http.get(url)
        
        .then(function(response) {
        
          $scope.nonefound = false; 
          
          if ($scope.searchTerm !== "") {
            
            $scope.pokemonfound = true;
            $scope.pokemonInfo = { 
                name: response.data.name,
                height: response.data.height,
                weight: response.data.weight,
                image: response.data.sprites.front_default
              }
            
            $scope.pokemons = null;
            $scope.allPokemons = false;
            $scope.previous = null;
            $scope.next = null;
            
          } else {
            
            $scope.pokemonfound = false;
            $scope.pokemons = response.data.results;
            $scope.allPokemons = true;
            $scope.previous = response.data.previous;
            $scope.next = response.data.next
            
          }
        
      }, function errorCallback() {
      
          let url = 'https://pokeapi.co/api/v2/pokemon-color/' + $scope.searchTerm;
      
          return $http.get(url).then(function(response) {
                                  
                                  $scope.pokemons = response.data.pokemon_species;
                                  $scope.allPokemons = false;
                                  $scope.nonefound = false;
            
                                }, function errorHandler() {
                                  $scope.nonefound = true;
                                  $scope.pokemons = null;
                                  $scope.allPokemons = false;
                                  $scope.pokemonfound = false;
                                  $scope.previous = null;
                                  $scope.next = null;
                                });
      });
    }
      
    this.init = function() {
      $scope.nonefound = false;
      $scope.pokemonfound = false;
      $scope.pokemonInfo = {};
      $scope.searchTerm = "";
      
      let url = 'https://pokeapi.co/api/v2/pokemon/';
      this.goTo(url);
    }
      
    this.goTo = function(address) {
      let url = address;
      return $http.get(url).then(function(response) {
        $scope.pokemons = response.data.results;
        $scope.allPokemons = true;
        $scope.previous = response.data.previous;
        $scope.next = response.data.next
      })
    }
      
    this.showPokemon = function(name) {
      let url = 'https://pokeapi.co/api/v2/pokemon/' + name;
      return $http.get(url)
      
      .then(function(response) {
        $scope.nonefound = false;
        $scope.pokemonfound = true;
        $scope.pokemonInfo = { 
          name: response.data.name,
          height: response.data.height,
          weight: response.data.weight,
          image: response.data.sprites.front_default
        }
  
        $scope.pokemons = null;
        $scope.allPokemons = false;
        $scope.previous = null;
        $scope.next = null;
      });
      
    }
    
    this.sortingToggle = function() {
      $scope.sortListAlphabetically = !$scope.sortListAlphabetically;
    }
      
  }]);