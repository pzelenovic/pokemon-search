### Pokemon Search

A simple, AngularJS, single page app, which allows you to search and view pokemons by consuming the  PokeAPI.

### Goal

Create simple single page web application in AngularJS (or your choice of
framework) that can search for and present Pokémon to the user.
Application should have landing page where user can search for Pokémon by name
or by color.
When search is done, user should be presented with a result on the same page. User
can sort results from the search query in alphabetical order.

### Additional acceptance criteria and instructions

1. When presenting Pokémon information, Pokémon image should be displayed. For
this you can use default sprite returned by the API.
2. Pokémon API can be found at: [PokéAPI](https://pokeapi.co/docs/v2.html). Specific ones
needed for this task are [here](https://pokeapi.co/docs/v2.html#pokemon-section) and
[here](https://pokeapi.co/docs/v2.html/#pok%C3%A9mon-colors).
3. You can use whatever libraries and tools available for your project (gulp, grunt,
yeoman etc).
4. Design and style of the application is up to you, but it should be easy to use.
5. Repository should have clear README on how to start the project.
6. Send us the link to the repository.
7. Bonus points: App is visible on some URL.
8. You don't have to do 100% of this test. Feel free to ask for help or clarification if
you're stuck.

### Use case examples

1. User can visit Pokémon application and search for bulbasaur. As a result user is
presented with information about the Pokémon .
2. User can visit Pokémon application and search for spiderman. As a result user is
presented with information that Pokémon does not exist.
3. User can visit Pokémon application and search for yellow. As a result user is
presented with collection of yellow Pokémon and can sort them in alphabetical order.

### Run the app locally

1. Clone this repo to your PC
2. Open the directory you just created in finder/explorer
3. Double click on index.html file